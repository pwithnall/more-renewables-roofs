MORE Renewables roofs
===

A survey of the Lancaster and Wyre area to decide which roofs are potentially suitable for installing solar systems on.

To view and edit the model, open `Roofs.qgz` in QGIS. You will need version 3.8 or newer.

Alternatively, the model may be viewed online at https://qgiscloud.com/pwithnall/Roofs/.

Contact
---

Philip Withnall <philip@tecnocode.co.uk>
