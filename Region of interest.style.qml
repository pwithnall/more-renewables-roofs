<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" minScale="1e+08" simplifyMaxScale="1" styleCategories="AllStyleCategories" simplifyLocal="1" version="3.4.9-Madeira" simplifyDrawingHints="1" simplifyDrawingTol="1" maxScale="0" simplifyAlgorithm="0" readOnly="0" labelsEnabled="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" symbollevels="0" forceraster="0" enableorderby="0">
    <symbols>
      <symbol type="fill" name="0" alpha="1" clip_to_extent="1" force_rhr="0">
        <layer class="SimpleFill" locked="0" enabled="1" pass="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="164,113,88,0" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="200,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="1" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties/>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks type="StringList">
      <Option type="QString" value=""/>
    </activeChecks>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="fid">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="full_id">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_id">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="admin_level">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="boundary">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="council_name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="council_style">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="designation">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ons_code">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ref:gss">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="source:ons_code">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="website">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wikidata">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wikipedia">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="layer">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="path">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="fid"/>
    <alias index="1" name="" field="full_id"/>
    <alias index="2" name="" field="osm_id"/>
    <alias index="3" name="" field="osm_type"/>
    <alias index="4" name="" field="admin_level"/>
    <alias index="5" name="" field="boundary"/>
    <alias index="6" name="" field="council_name"/>
    <alias index="7" name="" field="council_style"/>
    <alias index="8" name="" field="designation"/>
    <alias index="9" name="" field="name"/>
    <alias index="10" name="" field="ons_code"/>
    <alias index="11" name="" field="ref:gss"/>
    <alias index="12" name="" field="source:ons_code"/>
    <alias index="13" name="" field="type"/>
    <alias index="14" name="" field="website"/>
    <alias index="15" name="" field="wikidata"/>
    <alias index="16" name="" field="wikipedia"/>
    <alias index="17" name="" field="layer"/>
    <alias index="18" name="" field="path"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="fid"/>
    <default expression="" applyOnUpdate="0" field="full_id"/>
    <default expression="" applyOnUpdate="0" field="osm_id"/>
    <default expression="" applyOnUpdate="0" field="osm_type"/>
    <default expression="" applyOnUpdate="0" field="admin_level"/>
    <default expression="" applyOnUpdate="0" field="boundary"/>
    <default expression="" applyOnUpdate="0" field="council_name"/>
    <default expression="" applyOnUpdate="0" field="council_style"/>
    <default expression="" applyOnUpdate="0" field="designation"/>
    <default expression="" applyOnUpdate="0" field="name"/>
    <default expression="" applyOnUpdate="0" field="ons_code"/>
    <default expression="" applyOnUpdate="0" field="ref:gss"/>
    <default expression="" applyOnUpdate="0" field="source:ons_code"/>
    <default expression="" applyOnUpdate="0" field="type"/>
    <default expression="" applyOnUpdate="0" field="website"/>
    <default expression="" applyOnUpdate="0" field="wikidata"/>
    <default expression="" applyOnUpdate="0" field="wikipedia"/>
    <default expression="" applyOnUpdate="0" field="layer"/>
    <default expression="" applyOnUpdate="0" field="path"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fid"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="full_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="osm_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="osm_type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="admin_level"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="boundary"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="council_name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="council_style"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="designation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="ons_code"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="ref:gss"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="source:ons_code"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="website"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="wikidata"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="wikipedia"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="layer"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="path"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="fid"/>
    <constraint desc="" exp="" field="full_id"/>
    <constraint desc="" exp="" field="osm_id"/>
    <constraint desc="" exp="" field="osm_type"/>
    <constraint desc="" exp="" field="admin_level"/>
    <constraint desc="" exp="" field="boundary"/>
    <constraint desc="" exp="" field="council_name"/>
    <constraint desc="" exp="" field="council_style"/>
    <constraint desc="" exp="" field="designation"/>
    <constraint desc="" exp="" field="name"/>
    <constraint desc="" exp="" field="ons_code"/>
    <constraint desc="" exp="" field="ref:gss"/>
    <constraint desc="" exp="" field="source:ons_code"/>
    <constraint desc="" exp="" field="type"/>
    <constraint desc="" exp="" field="website"/>
    <constraint desc="" exp="" field="wikidata"/>
    <constraint desc="" exp="" field="wikipedia"/>
    <constraint desc="" exp="" field="layer"/>
    <constraint desc="" exp="" field="path"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns/>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable/>
  <labelOnTop/>
  <widgets/>
  <previewExpression></previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
