<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" minScale="1e+08" simplifyMaxScale="1" styleCategories="AllStyleCategories" simplifyLocal="1" version="3.4.9-Madeira" simplifyDrawingHints="1" simplifyDrawingTol="1" maxScale="0" simplifyAlgorithm="0" readOnly="0" labelsEnabled="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <renderer-v2 type="singleSymbol" symbollevels="0" forceraster="0" enableorderby="0">
    <symbols>
      <symbol type="fill" name="0" alpha="1" clip_to_extent="1" force_rhr="0">
        <layer class="SimpleFill" locked="0" enabled="1" pass="0">
          <prop v="3x:0,0,0,0,0,0" k="border_width_map_unit_scale"/>
          <prop v="114,155,111,255" k="color"/>
          <prop v="bevel" k="joinstyle"/>
          <prop v="0,0" k="offset"/>
          <prop v="3x:0,0,0,0,0,0" k="offset_map_unit_scale"/>
          <prop v="MM" k="offset_unit"/>
          <prop v="35,35,35,255" k="outline_color"/>
          <prop v="solid" k="outline_style"/>
          <prop v="0.26" k="outline_width"/>
          <prop v="MM" k="outline_width_unit"/>
          <prop v="solid" k="style"/>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
  </renderer-v2>
  <customproperties/>
  <blendMode>0</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks type="StringList">
      <Option type="QString" value=""/>
    </activeChecks>
    <checkConfiguration/>
  </geometryOptions>
  <fieldConfiguration>
    <field name="full_id">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_id">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="osm_type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:city">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:postcode">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:street">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fhrs:id">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="phone">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tourism">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="website">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wikidata">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="amenity">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:housename">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building:levels">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="residential">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:housenumber">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="designation">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="information">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="internet_access">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="internet_access:fee">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="level">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opening_hours">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="healthcare">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="healthcare:speciality">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="operator">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="roof:levels">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="roof:shape">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:flats">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="brand">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="brand:wikidata">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="brand:wikipedia">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:village">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="emergency">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building:flats">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="heritage">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:unit">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="shop">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="HE_ref">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="heritage:operator">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="listed_status">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building:use">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="source:addr">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wheelchair">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="alt_name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="int_name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="description">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="denomination">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="religion">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="access">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="historic">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wikipedia">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:country">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:hamlet">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="accommodation">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="brewery">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ele">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="food">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="outdoor_seating">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="smoking">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="castle_type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building:levels:underground">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fee">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="operator:type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="old_name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="alt_name:cs">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name:cs">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="man_made">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="landuse">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="toilets:wheelchair">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="layer">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="apartments">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="sport">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="disused">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building:name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="church">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="start_date">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:county">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="office">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="leisure">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="club">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:bicycle:retail">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="social_facility">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="social_facility:for">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diet:vegetarian">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="construction">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cuisine">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="disused:amenity">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fuel:GTL_diesel">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fuel:HGV_diesel">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fuel:biodiesel">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fuel:biogas">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fuel:diesel">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fuel:electricity">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fuel:lpg">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="highway">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="surface">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:place">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="beer_garden">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="freehouse">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="parking">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="real_ale">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="real_fire">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="loc_name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="Company">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="railway">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="height">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="postcode">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building:min_level">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="contact:facebook">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gay">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lgbtq">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="email">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tower">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandoned">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandoned:shop">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="preschool">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:units">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="roof:orientation">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="power">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ref">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="substation">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="industrial">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="product">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="noaddress">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:brakes">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:exhausts">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:repairs">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:servicing">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:tyres">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="community_centre:for">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opening_date">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="rooms">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service_times">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="factory">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="closed">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="oper">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="wifi">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="fax">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="second_hand">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:subdistrict">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="manufacturer:wikidata">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="shelter">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ruins">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="url">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diet:vegan">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="contact:phone">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="is_in">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="anglican">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="barrier">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="stars">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="source:amenity">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="source:name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bicycle_parking">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="covered">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="public_transport">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="toilets">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="toilets:access">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="toilets:changing_table">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="toilets:wheelchair:access">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="atm">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="atm:fee">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ref:navads">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="owner">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="room">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="disused:shop">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="takeaway">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="name:en">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opening_hours:url">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="games_room">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:suburb">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="toilets:disposal">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="community_centre">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="craft">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="maxheight">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="place">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="old_amenity">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="old_religion">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="source_ref">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="community">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="community:gender">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="unisex">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="delivery">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="credit_cards">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="mapillary">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="drive_through">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="construction_date">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="animal_shelter">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="studio">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="golf">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="house">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="disused:name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nursery">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="automated">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="noname">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="self_service">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="disused:office">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandoned:building">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="voltage">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:AtcoCode">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:Bearing">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:BusStopType">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:CommonName">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:Indicator">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:Landmark">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:NaptanCode">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:Street">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="naptan:verified">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="historic:railway">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:state">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:cards">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:cash">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="location">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="recycling_type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nohousenumber">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="historic:civilization">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="camra">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="capacity">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="dispensing">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:car_repair">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:new_car_sales">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:used_car_sales">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="microbrewery">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:body_repair">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:diagnostics">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:inspection">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="monitoring:weather">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gambling">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="construction_year">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tower:construction">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tower:type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="postal_code">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:interpolation">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building:colour">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="building:material">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="roof:colour">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="roof:material">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="cafe">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="content">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nat_ref">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="nam">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opening_hours:signed">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bunker_type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="military">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="roof:direction">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="government">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:street_1">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:MOT">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="service:vehicle:batteries">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="diaper">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:american_express">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:bancomat">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:bankaxess">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:coins">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:diners_club">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:discover_card">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:girocard">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:jcb">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:laser">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:maestro">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:mastercard">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:notes">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:visa">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:visa_debit">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:visa_electron">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:source">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ref:pol_id">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="payment:debit_cards">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="abandoned:man_made">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="gas_insulated">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="disused:operator">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="material">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="listed">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bench">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="shelter_type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="source:HE_ref">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="disused:building">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="tomb">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="water_point">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="waterway">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="snooker">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="lit">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="opening_hours:kitchen">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="disused:craft">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="artist_name">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="artwork_type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="ref:edubase">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="bin">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="indoor">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="telecom">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="water">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="generator:method">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="generator:type">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="plant:output:electricity">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="plant:source">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="reservation">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="addr:door">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field name="area">
      <editWidget type="">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias index="0" name="" field="full_id"/>
    <alias index="1" name="" field="osm_id"/>
    <alias index="2" name="" field="osm_type"/>
    <alias index="3" name="" field="addr:city"/>
    <alias index="4" name="" field="addr:postcode"/>
    <alias index="5" name="" field="addr:street"/>
    <alias index="6" name="" field="building"/>
    <alias index="7" name="" field="fhrs:id"/>
    <alias index="8" name="" field="name"/>
    <alias index="9" name="" field="phone"/>
    <alias index="10" name="" field="tourism"/>
    <alias index="11" name="" field="type"/>
    <alias index="12" name="" field="website"/>
    <alias index="13" name="" field="wikidata"/>
    <alias index="14" name="" field="amenity"/>
    <alias index="15" name="" field="addr:housename"/>
    <alias index="16" name="" field="building:levels"/>
    <alias index="17" name="" field="residential"/>
    <alias index="18" name="" field="addr:housenumber"/>
    <alias index="19" name="" field="designation"/>
    <alias index="20" name="" field="information"/>
    <alias index="21" name="" field="internet_access"/>
    <alias index="22" name="" field="internet_access:fee"/>
    <alias index="23" name="" field="level"/>
    <alias index="24" name="" field="opening_hours"/>
    <alias index="25" name="" field="healthcare"/>
    <alias index="26" name="" field="healthcare:speciality"/>
    <alias index="27" name="" field="operator"/>
    <alias index="28" name="" field="roof:levels"/>
    <alias index="29" name="" field="roof:shape"/>
    <alias index="30" name="" field="addr:flats"/>
    <alias index="31" name="" field="brand"/>
    <alias index="32" name="" field="brand:wikidata"/>
    <alias index="33" name="" field="brand:wikipedia"/>
    <alias index="34" name="" field="addr:village"/>
    <alias index="35" name="" field="emergency"/>
    <alias index="36" name="" field="building:flats"/>
    <alias index="37" name="" field="heritage"/>
    <alias index="38" name="" field="addr:unit"/>
    <alias index="39" name="" field="shop"/>
    <alias index="40" name="" field="HE_ref"/>
    <alias index="41" name="" field="heritage:operator"/>
    <alias index="42" name="" field="listed_status"/>
    <alias index="43" name="" field="building:use"/>
    <alias index="44" name="" field="source:addr"/>
    <alias index="45" name="" field="wheelchair"/>
    <alias index="46" name="" field="alt_name"/>
    <alias index="47" name="" field="int_name"/>
    <alias index="48" name="" field="description"/>
    <alias index="49" name="" field="denomination"/>
    <alias index="50" name="" field="religion"/>
    <alias index="51" name="" field="access"/>
    <alias index="52" name="" field="historic"/>
    <alias index="53" name="" field="wikipedia"/>
    <alias index="54" name="" field="addr:country"/>
    <alias index="55" name="" field="addr:hamlet"/>
    <alias index="56" name="" field="accommodation"/>
    <alias index="57" name="" field="brewery"/>
    <alias index="58" name="" field="ele"/>
    <alias index="59" name="" field="food"/>
    <alias index="60" name="" field="outdoor_seating"/>
    <alias index="61" name="" field="smoking"/>
    <alias index="62" name="" field="castle_type"/>
    <alias index="63" name="" field="building:levels:underground"/>
    <alias index="64" name="" field="fee"/>
    <alias index="65" name="" field="operator:type"/>
    <alias index="66" name="" field="old_name"/>
    <alias index="67" name="" field="alt_name:cs"/>
    <alias index="68" name="" field="name:cs"/>
    <alias index="69" name="" field="man_made"/>
    <alias index="70" name="" field="landuse"/>
    <alias index="71" name="" field="toilets:wheelchair"/>
    <alias index="72" name="" field="layer"/>
    <alias index="73" name="" field="apartments"/>
    <alias index="74" name="" field="sport"/>
    <alias index="75" name="" field="disused"/>
    <alias index="76" name="" field="building:name"/>
    <alias index="77" name="" field="church"/>
    <alias index="78" name="" field="start_date"/>
    <alias index="79" name="" field="addr:county"/>
    <alias index="80" name="" field="addr:name"/>
    <alias index="81" name="" field="office"/>
    <alias index="82" name="" field="leisure"/>
    <alias index="83" name="" field="club"/>
    <alias index="84" name="" field="service:bicycle:retail"/>
    <alias index="85" name="" field="social_facility"/>
    <alias index="86" name="" field="social_facility:for"/>
    <alias index="87" name="" field="diet:vegetarian"/>
    <alias index="88" name="" field="construction"/>
    <alias index="89" name="" field="cuisine"/>
    <alias index="90" name="" field="disused:amenity"/>
    <alias index="91" name="" field="fuel:GTL_diesel"/>
    <alias index="92" name="" field="fuel:HGV_diesel"/>
    <alias index="93" name="" field="fuel:biodiesel"/>
    <alias index="94" name="" field="fuel:biogas"/>
    <alias index="95" name="" field="fuel:diesel"/>
    <alias index="96" name="" field="fuel:electricity"/>
    <alias index="97" name="" field="fuel:lpg"/>
    <alias index="98" name="" field="highway"/>
    <alias index="99" name="" field="surface"/>
    <alias index="100" name="" field="addr:place"/>
    <alias index="101" name="" field="beer_garden"/>
    <alias index="102" name="" field="freehouse"/>
    <alias index="103" name="" field="parking"/>
    <alias index="104" name="" field="real_ale"/>
    <alias index="105" name="" field="real_fire"/>
    <alias index="106" name="" field="loc_name"/>
    <alias index="107" name="" field="Company"/>
    <alias index="108" name="" field="railway"/>
    <alias index="109" name="" field="height"/>
    <alias index="110" name="" field="postcode"/>
    <alias index="111" name="" field="building:min_level"/>
    <alias index="112" name="" field="contact:facebook"/>
    <alias index="113" name="" field="gay"/>
    <alias index="114" name="" field="lgbtq"/>
    <alias index="115" name="" field="email"/>
    <alias index="116" name="" field="tower"/>
    <alias index="117" name="" field="abandoned"/>
    <alias index="118" name="" field="abandoned:shop"/>
    <alias index="119" name="" field="preschool"/>
    <alias index="120" name="" field="addr:units"/>
    <alias index="121" name="" field="roof:orientation"/>
    <alias index="122" name="" field="power"/>
    <alias index="123" name="" field="ref"/>
    <alias index="124" name="" field="substation"/>
    <alias index="125" name="" field="industrial"/>
    <alias index="126" name="" field="product"/>
    <alias index="127" name="" field="noaddress"/>
    <alias index="128" name="" field="service:vehicle:brakes"/>
    <alias index="129" name="" field="service:vehicle:exhausts"/>
    <alias index="130" name="" field="service:vehicle:repairs"/>
    <alias index="131" name="" field="service:vehicle:servicing"/>
    <alias index="132" name="" field="service:vehicle:tyres"/>
    <alias index="133" name="" field="community_centre:for"/>
    <alias index="134" name="" field="opening_date"/>
    <alias index="135" name="" field="rooms"/>
    <alias index="136" name="" field="service_times"/>
    <alias index="137" name="" field="factory"/>
    <alias index="138" name="" field="closed"/>
    <alias index="139" name="" field="oper"/>
    <alias index="140" name="" field="wifi"/>
    <alias index="141" name="" field="fax"/>
    <alias index="142" name="" field="second_hand"/>
    <alias index="143" name="" field="addr:subdistrict"/>
    <alias index="144" name="" field="manufacturer:wikidata"/>
    <alias index="145" name="" field="shelter"/>
    <alias index="146" name="" field="ruins"/>
    <alias index="147" name="" field="url"/>
    <alias index="148" name="" field="diet:vegan"/>
    <alias index="149" name="" field="contact:phone"/>
    <alias index="150" name="" field="is_in"/>
    <alias index="151" name="" field="anglican"/>
    <alias index="152" name="" field="barrier"/>
    <alias index="153" name="" field="stars"/>
    <alias index="154" name="" field="source:amenity"/>
    <alias index="155" name="" field="source:name"/>
    <alias index="156" name="" field="bicycle_parking"/>
    <alias index="157" name="" field="covered"/>
    <alias index="158" name="" field="public_transport"/>
    <alias index="159" name="" field="toilets"/>
    <alias index="160" name="" field="toilets:access"/>
    <alias index="161" name="" field="toilets:changing_table"/>
    <alias index="162" name="" field="toilets:wheelchair:access"/>
    <alias index="163" name="" field="atm"/>
    <alias index="164" name="" field="atm:fee"/>
    <alias index="165" name="" field="ref:navads"/>
    <alias index="166" name="" field="owner"/>
    <alias index="167" name="" field="room"/>
    <alias index="168" name="" field="disused:shop"/>
    <alias index="169" name="" field="takeaway"/>
    <alias index="170" name="" field="name:en"/>
    <alias index="171" name="" field="opening_hours:url"/>
    <alias index="172" name="" field="games_room"/>
    <alias index="173" name="" field="addr:suburb"/>
    <alias index="174" name="" field="toilets:disposal"/>
    <alias index="175" name="" field="community_centre"/>
    <alias index="176" name="" field="craft"/>
    <alias index="177" name="" field="maxheight"/>
    <alias index="178" name="" field="place"/>
    <alias index="179" name="" field="old_amenity"/>
    <alias index="180" name="" field="old_religion"/>
    <alias index="181" name="" field="source_ref"/>
    <alias index="182" name="" field="community"/>
    <alias index="183" name="" field="community:gender"/>
    <alias index="184" name="" field="unisex"/>
    <alias index="185" name="" field="delivery"/>
    <alias index="186" name="" field="credit_cards"/>
    <alias index="187" name="" field="mapillary"/>
    <alias index="188" name="" field="drive_through"/>
    <alias index="189" name="" field="construction_date"/>
    <alias index="190" name="" field="animal_shelter"/>
    <alias index="191" name="" field="studio"/>
    <alias index="192" name="" field="golf"/>
    <alias index="193" name="" field="house"/>
    <alias index="194" name="" field="disused:name"/>
    <alias index="195" name="" field="nursery"/>
    <alias index="196" name="" field="automated"/>
    <alias index="197" name="" field="noname"/>
    <alias index="198" name="" field="self_service"/>
    <alias index="199" name="" field="disused:office"/>
    <alias index="200" name="" field="abandoned:building"/>
    <alias index="201" name="" field="voltage"/>
    <alias index="202" name="" field="naptan:AtcoCode"/>
    <alias index="203" name="" field="naptan:Bearing"/>
    <alias index="204" name="" field="naptan:BusStopType"/>
    <alias index="205" name="" field="naptan:CommonName"/>
    <alias index="206" name="" field="naptan:Indicator"/>
    <alias index="207" name="" field="naptan:Landmark"/>
    <alias index="208" name="" field="naptan:NaptanCode"/>
    <alias index="209" name="" field="naptan:Street"/>
    <alias index="210" name="" field="naptan:verified"/>
    <alias index="211" name="" field="historic:railway"/>
    <alias index="212" name="" field="addr:state"/>
    <alias index="213" name="" field="payment:cards"/>
    <alias index="214" name="" field="payment:cash"/>
    <alias index="215" name="" field="location"/>
    <alias index="216" name="" field="recycling_type"/>
    <alias index="217" name="" field="nohousenumber"/>
    <alias index="218" name="" field="historic:civilization"/>
    <alias index="219" name="" field="camra"/>
    <alias index="220" name="" field="capacity"/>
    <alias index="221" name="" field="dispensing"/>
    <alias index="222" name="" field="service:vehicle:car_repair"/>
    <alias index="223" name="" field="service:vehicle:new_car_sales"/>
    <alias index="224" name="" field="service:vehicle:used_car_sales"/>
    <alias index="225" name="" field="microbrewery"/>
    <alias index="226" name="" field="service:vehicle:body_repair"/>
    <alias index="227" name="" field="service:vehicle:diagnostics"/>
    <alias index="228" name="" field="service:vehicle:inspection"/>
    <alias index="229" name="" field="monitoring:weather"/>
    <alias index="230" name="" field="gambling"/>
    <alias index="231" name="" field="construction_year"/>
    <alias index="232" name="" field="tower:construction"/>
    <alias index="233" name="" field="tower:type"/>
    <alias index="234" name="" field="postal_code"/>
    <alias index="235" name="" field="addr:interpolation"/>
    <alias index="236" name="" field="building:colour"/>
    <alias index="237" name="" field="building:material"/>
    <alias index="238" name="" field="roof:colour"/>
    <alias index="239" name="" field="roof:material"/>
    <alias index="240" name="" field="cafe"/>
    <alias index="241" name="" field="content"/>
    <alias index="242" name="" field="nat_ref"/>
    <alias index="243" name="" field="nam"/>
    <alias index="244" name="" field="opening_hours:signed"/>
    <alias index="245" name="" field="bunker_type"/>
    <alias index="246" name="" field="military"/>
    <alias index="247" name="" field="roof:direction"/>
    <alias index="248" name="" field="government"/>
    <alias index="249" name="" field="addr:street_1"/>
    <alias index="250" name="" field="service:vehicle:MOT"/>
    <alias index="251" name="" field="service:vehicle:batteries"/>
    <alias index="252" name="" field="diaper"/>
    <alias index="253" name="" field="payment:american_express"/>
    <alias index="254" name="" field="payment:bancomat"/>
    <alias index="255" name="" field="payment:bankaxess"/>
    <alias index="256" name="" field="payment:coins"/>
    <alias index="257" name="" field="payment:diners_club"/>
    <alias index="258" name="" field="payment:discover_card"/>
    <alias index="259" name="" field="payment:girocard"/>
    <alias index="260" name="" field="payment:jcb"/>
    <alias index="261" name="" field="payment:laser"/>
    <alias index="262" name="" field="payment:maestro"/>
    <alias index="263" name="" field="payment:mastercard"/>
    <alias index="264" name="" field="payment:notes"/>
    <alias index="265" name="" field="payment:visa"/>
    <alias index="266" name="" field="payment:visa_debit"/>
    <alias index="267" name="" field="payment:visa_electron"/>
    <alias index="268" name="" field="addr:source"/>
    <alias index="269" name="" field="ref:pol_id"/>
    <alias index="270" name="" field="payment:debit_cards"/>
    <alias index="271" name="" field="abandoned:man_made"/>
    <alias index="272" name="" field="gas_insulated"/>
    <alias index="273" name="" field="disused:operator"/>
    <alias index="274" name="" field="material"/>
    <alias index="275" name="" field="listed"/>
    <alias index="276" name="" field="bench"/>
    <alias index="277" name="" field="shelter_type"/>
    <alias index="278" name="" field="source:HE_ref"/>
    <alias index="279" name="" field="disused:building"/>
    <alias index="280" name="" field="tomb"/>
    <alias index="281" name="" field="water_point"/>
    <alias index="282" name="" field="waterway"/>
    <alias index="283" name="" field="snooker"/>
    <alias index="284" name="" field="lit"/>
    <alias index="285" name="" field="opening_hours:kitchen"/>
    <alias index="286" name="" field="disused:craft"/>
    <alias index="287" name="" field="artist_name"/>
    <alias index="288" name="" field="artwork_type"/>
    <alias index="289" name="" field="ref:edubase"/>
    <alias index="290" name="" field="bin"/>
    <alias index="291" name="" field="indoor"/>
    <alias index="292" name="" field="telecom"/>
    <alias index="293" name="" field="water"/>
    <alias index="294" name="" field="generator:method"/>
    <alias index="295" name="" field="generator:type"/>
    <alias index="296" name="" field="plant:output:electricity"/>
    <alias index="297" name="" field="plant:source"/>
    <alias index="298" name="" field="reservation"/>
    <alias index="299" name="" field="addr:door"/>
    <alias index="300" name="" field="area"/>
  </aliases>
  <excludeAttributesWMS/>
  <excludeAttributesWFS/>
  <defaults>
    <default expression="" applyOnUpdate="0" field="full_id"/>
    <default expression="" applyOnUpdate="0" field="osm_id"/>
    <default expression="" applyOnUpdate="0" field="osm_type"/>
    <default expression="" applyOnUpdate="0" field="addr:city"/>
    <default expression="" applyOnUpdate="0" field="addr:postcode"/>
    <default expression="" applyOnUpdate="0" field="addr:street"/>
    <default expression="" applyOnUpdate="0" field="building"/>
    <default expression="" applyOnUpdate="0" field="fhrs:id"/>
    <default expression="" applyOnUpdate="0" field="name"/>
    <default expression="" applyOnUpdate="0" field="phone"/>
    <default expression="" applyOnUpdate="0" field="tourism"/>
    <default expression="" applyOnUpdate="0" field="type"/>
    <default expression="" applyOnUpdate="0" field="website"/>
    <default expression="" applyOnUpdate="0" field="wikidata"/>
    <default expression="" applyOnUpdate="0" field="amenity"/>
    <default expression="" applyOnUpdate="0" field="addr:housename"/>
    <default expression="" applyOnUpdate="0" field="building:levels"/>
    <default expression="" applyOnUpdate="0" field="residential"/>
    <default expression="" applyOnUpdate="0" field="addr:housenumber"/>
    <default expression="" applyOnUpdate="0" field="designation"/>
    <default expression="" applyOnUpdate="0" field="information"/>
    <default expression="" applyOnUpdate="0" field="internet_access"/>
    <default expression="" applyOnUpdate="0" field="internet_access:fee"/>
    <default expression="" applyOnUpdate="0" field="level"/>
    <default expression="" applyOnUpdate="0" field="opening_hours"/>
    <default expression="" applyOnUpdate="0" field="healthcare"/>
    <default expression="" applyOnUpdate="0" field="healthcare:speciality"/>
    <default expression="" applyOnUpdate="0" field="operator"/>
    <default expression="" applyOnUpdate="0" field="roof:levels"/>
    <default expression="" applyOnUpdate="0" field="roof:shape"/>
    <default expression="" applyOnUpdate="0" field="addr:flats"/>
    <default expression="" applyOnUpdate="0" field="brand"/>
    <default expression="" applyOnUpdate="0" field="brand:wikidata"/>
    <default expression="" applyOnUpdate="0" field="brand:wikipedia"/>
    <default expression="" applyOnUpdate="0" field="addr:village"/>
    <default expression="" applyOnUpdate="0" field="emergency"/>
    <default expression="" applyOnUpdate="0" field="building:flats"/>
    <default expression="" applyOnUpdate="0" field="heritage"/>
    <default expression="" applyOnUpdate="0" field="addr:unit"/>
    <default expression="" applyOnUpdate="0" field="shop"/>
    <default expression="" applyOnUpdate="0" field="HE_ref"/>
    <default expression="" applyOnUpdate="0" field="heritage:operator"/>
    <default expression="" applyOnUpdate="0" field="listed_status"/>
    <default expression="" applyOnUpdate="0" field="building:use"/>
    <default expression="" applyOnUpdate="0" field="source:addr"/>
    <default expression="" applyOnUpdate="0" field="wheelchair"/>
    <default expression="" applyOnUpdate="0" field="alt_name"/>
    <default expression="" applyOnUpdate="0" field="int_name"/>
    <default expression="" applyOnUpdate="0" field="description"/>
    <default expression="" applyOnUpdate="0" field="denomination"/>
    <default expression="" applyOnUpdate="0" field="religion"/>
    <default expression="" applyOnUpdate="0" field="access"/>
    <default expression="" applyOnUpdate="0" field="historic"/>
    <default expression="" applyOnUpdate="0" field="wikipedia"/>
    <default expression="" applyOnUpdate="0" field="addr:country"/>
    <default expression="" applyOnUpdate="0" field="addr:hamlet"/>
    <default expression="" applyOnUpdate="0" field="accommodation"/>
    <default expression="" applyOnUpdate="0" field="brewery"/>
    <default expression="" applyOnUpdate="0" field="ele"/>
    <default expression="" applyOnUpdate="0" field="food"/>
    <default expression="" applyOnUpdate="0" field="outdoor_seating"/>
    <default expression="" applyOnUpdate="0" field="smoking"/>
    <default expression="" applyOnUpdate="0" field="castle_type"/>
    <default expression="" applyOnUpdate="0" field="building:levels:underground"/>
    <default expression="" applyOnUpdate="0" field="fee"/>
    <default expression="" applyOnUpdate="0" field="operator:type"/>
    <default expression="" applyOnUpdate="0" field="old_name"/>
    <default expression="" applyOnUpdate="0" field="alt_name:cs"/>
    <default expression="" applyOnUpdate="0" field="name:cs"/>
    <default expression="" applyOnUpdate="0" field="man_made"/>
    <default expression="" applyOnUpdate="0" field="landuse"/>
    <default expression="" applyOnUpdate="0" field="toilets:wheelchair"/>
    <default expression="" applyOnUpdate="0" field="layer"/>
    <default expression="" applyOnUpdate="0" field="apartments"/>
    <default expression="" applyOnUpdate="0" field="sport"/>
    <default expression="" applyOnUpdate="0" field="disused"/>
    <default expression="" applyOnUpdate="0" field="building:name"/>
    <default expression="" applyOnUpdate="0" field="church"/>
    <default expression="" applyOnUpdate="0" field="start_date"/>
    <default expression="" applyOnUpdate="0" field="addr:county"/>
    <default expression="" applyOnUpdate="0" field="addr:name"/>
    <default expression="" applyOnUpdate="0" field="office"/>
    <default expression="" applyOnUpdate="0" field="leisure"/>
    <default expression="" applyOnUpdate="0" field="club"/>
    <default expression="" applyOnUpdate="0" field="service:bicycle:retail"/>
    <default expression="" applyOnUpdate="0" field="social_facility"/>
    <default expression="" applyOnUpdate="0" field="social_facility:for"/>
    <default expression="" applyOnUpdate="0" field="diet:vegetarian"/>
    <default expression="" applyOnUpdate="0" field="construction"/>
    <default expression="" applyOnUpdate="0" field="cuisine"/>
    <default expression="" applyOnUpdate="0" field="disused:amenity"/>
    <default expression="" applyOnUpdate="0" field="fuel:GTL_diesel"/>
    <default expression="" applyOnUpdate="0" field="fuel:HGV_diesel"/>
    <default expression="" applyOnUpdate="0" field="fuel:biodiesel"/>
    <default expression="" applyOnUpdate="0" field="fuel:biogas"/>
    <default expression="" applyOnUpdate="0" field="fuel:diesel"/>
    <default expression="" applyOnUpdate="0" field="fuel:electricity"/>
    <default expression="" applyOnUpdate="0" field="fuel:lpg"/>
    <default expression="" applyOnUpdate="0" field="highway"/>
    <default expression="" applyOnUpdate="0" field="surface"/>
    <default expression="" applyOnUpdate="0" field="addr:place"/>
    <default expression="" applyOnUpdate="0" field="beer_garden"/>
    <default expression="" applyOnUpdate="0" field="freehouse"/>
    <default expression="" applyOnUpdate="0" field="parking"/>
    <default expression="" applyOnUpdate="0" field="real_ale"/>
    <default expression="" applyOnUpdate="0" field="real_fire"/>
    <default expression="" applyOnUpdate="0" field="loc_name"/>
    <default expression="" applyOnUpdate="0" field="Company"/>
    <default expression="" applyOnUpdate="0" field="railway"/>
    <default expression="" applyOnUpdate="0" field="height"/>
    <default expression="" applyOnUpdate="0" field="postcode"/>
    <default expression="" applyOnUpdate="0" field="building:min_level"/>
    <default expression="" applyOnUpdate="0" field="contact:facebook"/>
    <default expression="" applyOnUpdate="0" field="gay"/>
    <default expression="" applyOnUpdate="0" field="lgbtq"/>
    <default expression="" applyOnUpdate="0" field="email"/>
    <default expression="" applyOnUpdate="0" field="tower"/>
    <default expression="" applyOnUpdate="0" field="abandoned"/>
    <default expression="" applyOnUpdate="0" field="abandoned:shop"/>
    <default expression="" applyOnUpdate="0" field="preschool"/>
    <default expression="" applyOnUpdate="0" field="addr:units"/>
    <default expression="" applyOnUpdate="0" field="roof:orientation"/>
    <default expression="" applyOnUpdate="0" field="power"/>
    <default expression="" applyOnUpdate="0" field="ref"/>
    <default expression="" applyOnUpdate="0" field="substation"/>
    <default expression="" applyOnUpdate="0" field="industrial"/>
    <default expression="" applyOnUpdate="0" field="product"/>
    <default expression="" applyOnUpdate="0" field="noaddress"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:brakes"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:exhausts"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:repairs"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:servicing"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:tyres"/>
    <default expression="" applyOnUpdate="0" field="community_centre:for"/>
    <default expression="" applyOnUpdate="0" field="opening_date"/>
    <default expression="" applyOnUpdate="0" field="rooms"/>
    <default expression="" applyOnUpdate="0" field="service_times"/>
    <default expression="" applyOnUpdate="0" field="factory"/>
    <default expression="" applyOnUpdate="0" field="closed"/>
    <default expression="" applyOnUpdate="0" field="oper"/>
    <default expression="" applyOnUpdate="0" field="wifi"/>
    <default expression="" applyOnUpdate="0" field="fax"/>
    <default expression="" applyOnUpdate="0" field="second_hand"/>
    <default expression="" applyOnUpdate="0" field="addr:subdistrict"/>
    <default expression="" applyOnUpdate="0" field="manufacturer:wikidata"/>
    <default expression="" applyOnUpdate="0" field="shelter"/>
    <default expression="" applyOnUpdate="0" field="ruins"/>
    <default expression="" applyOnUpdate="0" field="url"/>
    <default expression="" applyOnUpdate="0" field="diet:vegan"/>
    <default expression="" applyOnUpdate="0" field="contact:phone"/>
    <default expression="" applyOnUpdate="0" field="is_in"/>
    <default expression="" applyOnUpdate="0" field="anglican"/>
    <default expression="" applyOnUpdate="0" field="barrier"/>
    <default expression="" applyOnUpdate="0" field="stars"/>
    <default expression="" applyOnUpdate="0" field="source:amenity"/>
    <default expression="" applyOnUpdate="0" field="source:name"/>
    <default expression="" applyOnUpdate="0" field="bicycle_parking"/>
    <default expression="" applyOnUpdate="0" field="covered"/>
    <default expression="" applyOnUpdate="0" field="public_transport"/>
    <default expression="" applyOnUpdate="0" field="toilets"/>
    <default expression="" applyOnUpdate="0" field="toilets:access"/>
    <default expression="" applyOnUpdate="0" field="toilets:changing_table"/>
    <default expression="" applyOnUpdate="0" field="toilets:wheelchair:access"/>
    <default expression="" applyOnUpdate="0" field="atm"/>
    <default expression="" applyOnUpdate="0" field="atm:fee"/>
    <default expression="" applyOnUpdate="0" field="ref:navads"/>
    <default expression="" applyOnUpdate="0" field="owner"/>
    <default expression="" applyOnUpdate="0" field="room"/>
    <default expression="" applyOnUpdate="0" field="disused:shop"/>
    <default expression="" applyOnUpdate="0" field="takeaway"/>
    <default expression="" applyOnUpdate="0" field="name:en"/>
    <default expression="" applyOnUpdate="0" field="opening_hours:url"/>
    <default expression="" applyOnUpdate="0" field="games_room"/>
    <default expression="" applyOnUpdate="0" field="addr:suburb"/>
    <default expression="" applyOnUpdate="0" field="toilets:disposal"/>
    <default expression="" applyOnUpdate="0" field="community_centre"/>
    <default expression="" applyOnUpdate="0" field="craft"/>
    <default expression="" applyOnUpdate="0" field="maxheight"/>
    <default expression="" applyOnUpdate="0" field="place"/>
    <default expression="" applyOnUpdate="0" field="old_amenity"/>
    <default expression="" applyOnUpdate="0" field="old_religion"/>
    <default expression="" applyOnUpdate="0" field="source_ref"/>
    <default expression="" applyOnUpdate="0" field="community"/>
    <default expression="" applyOnUpdate="0" field="community:gender"/>
    <default expression="" applyOnUpdate="0" field="unisex"/>
    <default expression="" applyOnUpdate="0" field="delivery"/>
    <default expression="" applyOnUpdate="0" field="credit_cards"/>
    <default expression="" applyOnUpdate="0" field="mapillary"/>
    <default expression="" applyOnUpdate="0" field="drive_through"/>
    <default expression="" applyOnUpdate="0" field="construction_date"/>
    <default expression="" applyOnUpdate="0" field="animal_shelter"/>
    <default expression="" applyOnUpdate="0" field="studio"/>
    <default expression="" applyOnUpdate="0" field="golf"/>
    <default expression="" applyOnUpdate="0" field="house"/>
    <default expression="" applyOnUpdate="0" field="disused:name"/>
    <default expression="" applyOnUpdate="0" field="nursery"/>
    <default expression="" applyOnUpdate="0" field="automated"/>
    <default expression="" applyOnUpdate="0" field="noname"/>
    <default expression="" applyOnUpdate="0" field="self_service"/>
    <default expression="" applyOnUpdate="0" field="disused:office"/>
    <default expression="" applyOnUpdate="0" field="abandoned:building"/>
    <default expression="" applyOnUpdate="0" field="voltage"/>
    <default expression="" applyOnUpdate="0" field="naptan:AtcoCode"/>
    <default expression="" applyOnUpdate="0" field="naptan:Bearing"/>
    <default expression="" applyOnUpdate="0" field="naptan:BusStopType"/>
    <default expression="" applyOnUpdate="0" field="naptan:CommonName"/>
    <default expression="" applyOnUpdate="0" field="naptan:Indicator"/>
    <default expression="" applyOnUpdate="0" field="naptan:Landmark"/>
    <default expression="" applyOnUpdate="0" field="naptan:NaptanCode"/>
    <default expression="" applyOnUpdate="0" field="naptan:Street"/>
    <default expression="" applyOnUpdate="0" field="naptan:verified"/>
    <default expression="" applyOnUpdate="0" field="historic:railway"/>
    <default expression="" applyOnUpdate="0" field="addr:state"/>
    <default expression="" applyOnUpdate="0" field="payment:cards"/>
    <default expression="" applyOnUpdate="0" field="payment:cash"/>
    <default expression="" applyOnUpdate="0" field="location"/>
    <default expression="" applyOnUpdate="0" field="recycling_type"/>
    <default expression="" applyOnUpdate="0" field="nohousenumber"/>
    <default expression="" applyOnUpdate="0" field="historic:civilization"/>
    <default expression="" applyOnUpdate="0" field="camra"/>
    <default expression="" applyOnUpdate="0" field="capacity"/>
    <default expression="" applyOnUpdate="0" field="dispensing"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:car_repair"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:new_car_sales"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:used_car_sales"/>
    <default expression="" applyOnUpdate="0" field="microbrewery"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:body_repair"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:diagnostics"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:inspection"/>
    <default expression="" applyOnUpdate="0" field="monitoring:weather"/>
    <default expression="" applyOnUpdate="0" field="gambling"/>
    <default expression="" applyOnUpdate="0" field="construction_year"/>
    <default expression="" applyOnUpdate="0" field="tower:construction"/>
    <default expression="" applyOnUpdate="0" field="tower:type"/>
    <default expression="" applyOnUpdate="0" field="postal_code"/>
    <default expression="" applyOnUpdate="0" field="addr:interpolation"/>
    <default expression="" applyOnUpdate="0" field="building:colour"/>
    <default expression="" applyOnUpdate="0" field="building:material"/>
    <default expression="" applyOnUpdate="0" field="roof:colour"/>
    <default expression="" applyOnUpdate="0" field="roof:material"/>
    <default expression="" applyOnUpdate="0" field="cafe"/>
    <default expression="" applyOnUpdate="0" field="content"/>
    <default expression="" applyOnUpdate="0" field="nat_ref"/>
    <default expression="" applyOnUpdate="0" field="nam"/>
    <default expression="" applyOnUpdate="0" field="opening_hours:signed"/>
    <default expression="" applyOnUpdate="0" field="bunker_type"/>
    <default expression="" applyOnUpdate="0" field="military"/>
    <default expression="" applyOnUpdate="0" field="roof:direction"/>
    <default expression="" applyOnUpdate="0" field="government"/>
    <default expression="" applyOnUpdate="0" field="addr:street_1"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:MOT"/>
    <default expression="" applyOnUpdate="0" field="service:vehicle:batteries"/>
    <default expression="" applyOnUpdate="0" field="diaper"/>
    <default expression="" applyOnUpdate="0" field="payment:american_express"/>
    <default expression="" applyOnUpdate="0" field="payment:bancomat"/>
    <default expression="" applyOnUpdate="0" field="payment:bankaxess"/>
    <default expression="" applyOnUpdate="0" field="payment:coins"/>
    <default expression="" applyOnUpdate="0" field="payment:diners_club"/>
    <default expression="" applyOnUpdate="0" field="payment:discover_card"/>
    <default expression="" applyOnUpdate="0" field="payment:girocard"/>
    <default expression="" applyOnUpdate="0" field="payment:jcb"/>
    <default expression="" applyOnUpdate="0" field="payment:laser"/>
    <default expression="" applyOnUpdate="0" field="payment:maestro"/>
    <default expression="" applyOnUpdate="0" field="payment:mastercard"/>
    <default expression="" applyOnUpdate="0" field="payment:notes"/>
    <default expression="" applyOnUpdate="0" field="payment:visa"/>
    <default expression="" applyOnUpdate="0" field="payment:visa_debit"/>
    <default expression="" applyOnUpdate="0" field="payment:visa_electron"/>
    <default expression="" applyOnUpdate="0" field="addr:source"/>
    <default expression="" applyOnUpdate="0" field="ref:pol_id"/>
    <default expression="" applyOnUpdate="0" field="payment:debit_cards"/>
    <default expression="" applyOnUpdate="0" field="abandoned:man_made"/>
    <default expression="" applyOnUpdate="0" field="gas_insulated"/>
    <default expression="" applyOnUpdate="0" field="disused:operator"/>
    <default expression="" applyOnUpdate="0" field="material"/>
    <default expression="" applyOnUpdate="0" field="listed"/>
    <default expression="" applyOnUpdate="0" field="bench"/>
    <default expression="" applyOnUpdate="0" field="shelter_type"/>
    <default expression="" applyOnUpdate="0" field="source:HE_ref"/>
    <default expression="" applyOnUpdate="0" field="disused:building"/>
    <default expression="" applyOnUpdate="0" field="tomb"/>
    <default expression="" applyOnUpdate="0" field="water_point"/>
    <default expression="" applyOnUpdate="0" field="waterway"/>
    <default expression="" applyOnUpdate="0" field="snooker"/>
    <default expression="" applyOnUpdate="0" field="lit"/>
    <default expression="" applyOnUpdate="0" field="opening_hours:kitchen"/>
    <default expression="" applyOnUpdate="0" field="disused:craft"/>
    <default expression="" applyOnUpdate="0" field="artist_name"/>
    <default expression="" applyOnUpdate="0" field="artwork_type"/>
    <default expression="" applyOnUpdate="0" field="ref:edubase"/>
    <default expression="" applyOnUpdate="0" field="bin"/>
    <default expression="" applyOnUpdate="0" field="indoor"/>
    <default expression="" applyOnUpdate="0" field="telecom"/>
    <default expression="" applyOnUpdate="0" field="water"/>
    <default expression="" applyOnUpdate="0" field="generator:method"/>
    <default expression="" applyOnUpdate="0" field="generator:type"/>
    <default expression="" applyOnUpdate="0" field="plant:output:electricity"/>
    <default expression="" applyOnUpdate="0" field="plant:source"/>
    <default expression="" applyOnUpdate="0" field="reservation"/>
    <default expression="" applyOnUpdate="0" field="addr:door"/>
    <default expression="" applyOnUpdate="0" field="area"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="full_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="osm_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="osm_type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:city"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:postcode"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:street"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fhrs:id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="phone"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="tourism"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="website"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="wikidata"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="amenity"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:housename"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building:levels"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="residential"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:housenumber"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="designation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="information"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="internet_access"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="internet_access:fee"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="level"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="opening_hours"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="healthcare"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="healthcare:speciality"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="operator"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="roof:levels"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="roof:shape"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:flats"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="brand"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="brand:wikidata"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="brand:wikipedia"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:village"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="emergency"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building:flats"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="heritage"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:unit"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="shop"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="HE_ref"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="heritage:operator"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="listed_status"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building:use"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="source:addr"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="wheelchair"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="alt_name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="int_name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="description"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="denomination"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="religion"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="access"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="historic"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="wikipedia"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:country"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:hamlet"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="accommodation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="brewery"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="ele"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="food"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="outdoor_seating"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="smoking"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="castle_type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building:levels:underground"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fee"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="operator:type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="old_name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="alt_name:cs"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="name:cs"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="man_made"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="landuse"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="toilets:wheelchair"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="layer"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="apartments"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="sport"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="disused"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building:name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="church"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="start_date"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:county"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="office"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="leisure"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="club"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:bicycle:retail"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="social_facility"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="social_facility:for"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="diet:vegetarian"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="construction"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="cuisine"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="disused:amenity"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fuel:GTL_diesel"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fuel:HGV_diesel"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fuel:biodiesel"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fuel:biogas"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fuel:diesel"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fuel:electricity"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fuel:lpg"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="highway"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="surface"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:place"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="beer_garden"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="freehouse"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="parking"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="real_ale"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="real_fire"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="loc_name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="Company"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="railway"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="height"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="postcode"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building:min_level"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="contact:facebook"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="gay"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="lgbtq"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="email"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="tower"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="abandoned"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="abandoned:shop"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="preschool"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:units"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="roof:orientation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="power"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="ref"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="substation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="industrial"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="product"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="noaddress"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:brakes"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:exhausts"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:repairs"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:servicing"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:tyres"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="community_centre:for"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="opening_date"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="rooms"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service_times"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="factory"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="closed"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="oper"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="wifi"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="fax"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="second_hand"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:subdistrict"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="manufacturer:wikidata"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="shelter"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="ruins"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="url"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="diet:vegan"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="contact:phone"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="is_in"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="anglican"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="barrier"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="stars"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="source:amenity"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="source:name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="bicycle_parking"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="covered"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="public_transport"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="toilets"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="toilets:access"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="toilets:changing_table"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="toilets:wheelchair:access"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="atm"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="atm:fee"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="ref:navads"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="owner"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="room"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="disused:shop"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="takeaway"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="name:en"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="opening_hours:url"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="games_room"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:suburb"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="toilets:disposal"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="community_centre"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="craft"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="maxheight"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="place"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="old_amenity"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="old_religion"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="source_ref"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="community"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="community:gender"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="unisex"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="delivery"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="credit_cards"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="mapillary"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="drive_through"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="construction_date"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="animal_shelter"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="studio"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="golf"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="house"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="disused:name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="nursery"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="automated"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="noname"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="self_service"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="disused:office"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="abandoned:building"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="voltage"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:AtcoCode"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:Bearing"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:BusStopType"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:CommonName"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:Indicator"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:Landmark"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:NaptanCode"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:Street"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="naptan:verified"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="historic:railway"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:state"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:cards"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:cash"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="location"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="recycling_type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="nohousenumber"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="historic:civilization"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="camra"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="capacity"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="dispensing"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:car_repair"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:new_car_sales"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:used_car_sales"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="microbrewery"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:body_repair"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:diagnostics"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:inspection"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="monitoring:weather"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="gambling"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="construction_year"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="tower:construction"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="tower:type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="postal_code"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:interpolation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building:colour"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="building:material"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="roof:colour"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="roof:material"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="cafe"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="content"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="nat_ref"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="nam"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="opening_hours:signed"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="bunker_type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="military"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="roof:direction"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="government"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:street_1"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:MOT"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="service:vehicle:batteries"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="diaper"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:american_express"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:bancomat"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:bankaxess"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:coins"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:diners_club"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:discover_card"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:girocard"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:jcb"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:laser"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:maestro"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:mastercard"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:notes"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:visa"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:visa_debit"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:visa_electron"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:source"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="ref:pol_id"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="payment:debit_cards"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="abandoned:man_made"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="gas_insulated"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="disused:operator"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="material"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="listed"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="bench"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="shelter_type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="source:HE_ref"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="disused:building"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="tomb"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="water_point"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="waterway"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="snooker"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="lit"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="opening_hours:kitchen"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="disused:craft"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="artist_name"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="artwork_type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="ref:edubase"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="bin"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="indoor"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="telecom"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="water"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="generator:method"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="generator:type"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="plant:output:electricity"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="plant:source"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="reservation"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="addr:door"/>
    <constraint unique_strength="0" notnull_strength="0" exp_strength="0" constraints="0" field="area"/>
  </constraints>
  <constraintExpressions>
    <constraint desc="" exp="" field="full_id"/>
    <constraint desc="" exp="" field="osm_id"/>
    <constraint desc="" exp="" field="osm_type"/>
    <constraint desc="" exp="" field="addr:city"/>
    <constraint desc="" exp="" field="addr:postcode"/>
    <constraint desc="" exp="" field="addr:street"/>
    <constraint desc="" exp="" field="building"/>
    <constraint desc="" exp="" field="fhrs:id"/>
    <constraint desc="" exp="" field="name"/>
    <constraint desc="" exp="" field="phone"/>
    <constraint desc="" exp="" field="tourism"/>
    <constraint desc="" exp="" field="type"/>
    <constraint desc="" exp="" field="website"/>
    <constraint desc="" exp="" field="wikidata"/>
    <constraint desc="" exp="" field="amenity"/>
    <constraint desc="" exp="" field="addr:housename"/>
    <constraint desc="" exp="" field="building:levels"/>
    <constraint desc="" exp="" field="residential"/>
    <constraint desc="" exp="" field="addr:housenumber"/>
    <constraint desc="" exp="" field="designation"/>
    <constraint desc="" exp="" field="information"/>
    <constraint desc="" exp="" field="internet_access"/>
    <constraint desc="" exp="" field="internet_access:fee"/>
    <constraint desc="" exp="" field="level"/>
    <constraint desc="" exp="" field="opening_hours"/>
    <constraint desc="" exp="" field="healthcare"/>
    <constraint desc="" exp="" field="healthcare:speciality"/>
    <constraint desc="" exp="" field="operator"/>
    <constraint desc="" exp="" field="roof:levels"/>
    <constraint desc="" exp="" field="roof:shape"/>
    <constraint desc="" exp="" field="addr:flats"/>
    <constraint desc="" exp="" field="brand"/>
    <constraint desc="" exp="" field="brand:wikidata"/>
    <constraint desc="" exp="" field="brand:wikipedia"/>
    <constraint desc="" exp="" field="addr:village"/>
    <constraint desc="" exp="" field="emergency"/>
    <constraint desc="" exp="" field="building:flats"/>
    <constraint desc="" exp="" field="heritage"/>
    <constraint desc="" exp="" field="addr:unit"/>
    <constraint desc="" exp="" field="shop"/>
    <constraint desc="" exp="" field="HE_ref"/>
    <constraint desc="" exp="" field="heritage:operator"/>
    <constraint desc="" exp="" field="listed_status"/>
    <constraint desc="" exp="" field="building:use"/>
    <constraint desc="" exp="" field="source:addr"/>
    <constraint desc="" exp="" field="wheelchair"/>
    <constraint desc="" exp="" field="alt_name"/>
    <constraint desc="" exp="" field="int_name"/>
    <constraint desc="" exp="" field="description"/>
    <constraint desc="" exp="" field="denomination"/>
    <constraint desc="" exp="" field="religion"/>
    <constraint desc="" exp="" field="access"/>
    <constraint desc="" exp="" field="historic"/>
    <constraint desc="" exp="" field="wikipedia"/>
    <constraint desc="" exp="" field="addr:country"/>
    <constraint desc="" exp="" field="addr:hamlet"/>
    <constraint desc="" exp="" field="accommodation"/>
    <constraint desc="" exp="" field="brewery"/>
    <constraint desc="" exp="" field="ele"/>
    <constraint desc="" exp="" field="food"/>
    <constraint desc="" exp="" field="outdoor_seating"/>
    <constraint desc="" exp="" field="smoking"/>
    <constraint desc="" exp="" field="castle_type"/>
    <constraint desc="" exp="" field="building:levels:underground"/>
    <constraint desc="" exp="" field="fee"/>
    <constraint desc="" exp="" field="operator:type"/>
    <constraint desc="" exp="" field="old_name"/>
    <constraint desc="" exp="" field="alt_name:cs"/>
    <constraint desc="" exp="" field="name:cs"/>
    <constraint desc="" exp="" field="man_made"/>
    <constraint desc="" exp="" field="landuse"/>
    <constraint desc="" exp="" field="toilets:wheelchair"/>
    <constraint desc="" exp="" field="layer"/>
    <constraint desc="" exp="" field="apartments"/>
    <constraint desc="" exp="" field="sport"/>
    <constraint desc="" exp="" field="disused"/>
    <constraint desc="" exp="" field="building:name"/>
    <constraint desc="" exp="" field="church"/>
    <constraint desc="" exp="" field="start_date"/>
    <constraint desc="" exp="" field="addr:county"/>
    <constraint desc="" exp="" field="addr:name"/>
    <constraint desc="" exp="" field="office"/>
    <constraint desc="" exp="" field="leisure"/>
    <constraint desc="" exp="" field="club"/>
    <constraint desc="" exp="" field="service:bicycle:retail"/>
    <constraint desc="" exp="" field="social_facility"/>
    <constraint desc="" exp="" field="social_facility:for"/>
    <constraint desc="" exp="" field="diet:vegetarian"/>
    <constraint desc="" exp="" field="construction"/>
    <constraint desc="" exp="" field="cuisine"/>
    <constraint desc="" exp="" field="disused:amenity"/>
    <constraint desc="" exp="" field="fuel:GTL_diesel"/>
    <constraint desc="" exp="" field="fuel:HGV_diesel"/>
    <constraint desc="" exp="" field="fuel:biodiesel"/>
    <constraint desc="" exp="" field="fuel:biogas"/>
    <constraint desc="" exp="" field="fuel:diesel"/>
    <constraint desc="" exp="" field="fuel:electricity"/>
    <constraint desc="" exp="" field="fuel:lpg"/>
    <constraint desc="" exp="" field="highway"/>
    <constraint desc="" exp="" field="surface"/>
    <constraint desc="" exp="" field="addr:place"/>
    <constraint desc="" exp="" field="beer_garden"/>
    <constraint desc="" exp="" field="freehouse"/>
    <constraint desc="" exp="" field="parking"/>
    <constraint desc="" exp="" field="real_ale"/>
    <constraint desc="" exp="" field="real_fire"/>
    <constraint desc="" exp="" field="loc_name"/>
    <constraint desc="" exp="" field="Company"/>
    <constraint desc="" exp="" field="railway"/>
    <constraint desc="" exp="" field="height"/>
    <constraint desc="" exp="" field="postcode"/>
    <constraint desc="" exp="" field="building:min_level"/>
    <constraint desc="" exp="" field="contact:facebook"/>
    <constraint desc="" exp="" field="gay"/>
    <constraint desc="" exp="" field="lgbtq"/>
    <constraint desc="" exp="" field="email"/>
    <constraint desc="" exp="" field="tower"/>
    <constraint desc="" exp="" field="abandoned"/>
    <constraint desc="" exp="" field="abandoned:shop"/>
    <constraint desc="" exp="" field="preschool"/>
    <constraint desc="" exp="" field="addr:units"/>
    <constraint desc="" exp="" field="roof:orientation"/>
    <constraint desc="" exp="" field="power"/>
    <constraint desc="" exp="" field="ref"/>
    <constraint desc="" exp="" field="substation"/>
    <constraint desc="" exp="" field="industrial"/>
    <constraint desc="" exp="" field="product"/>
    <constraint desc="" exp="" field="noaddress"/>
    <constraint desc="" exp="" field="service:vehicle:brakes"/>
    <constraint desc="" exp="" field="service:vehicle:exhausts"/>
    <constraint desc="" exp="" field="service:vehicle:repairs"/>
    <constraint desc="" exp="" field="service:vehicle:servicing"/>
    <constraint desc="" exp="" field="service:vehicle:tyres"/>
    <constraint desc="" exp="" field="community_centre:for"/>
    <constraint desc="" exp="" field="opening_date"/>
    <constraint desc="" exp="" field="rooms"/>
    <constraint desc="" exp="" field="service_times"/>
    <constraint desc="" exp="" field="factory"/>
    <constraint desc="" exp="" field="closed"/>
    <constraint desc="" exp="" field="oper"/>
    <constraint desc="" exp="" field="wifi"/>
    <constraint desc="" exp="" field="fax"/>
    <constraint desc="" exp="" field="second_hand"/>
    <constraint desc="" exp="" field="addr:subdistrict"/>
    <constraint desc="" exp="" field="manufacturer:wikidata"/>
    <constraint desc="" exp="" field="shelter"/>
    <constraint desc="" exp="" field="ruins"/>
    <constraint desc="" exp="" field="url"/>
    <constraint desc="" exp="" field="diet:vegan"/>
    <constraint desc="" exp="" field="contact:phone"/>
    <constraint desc="" exp="" field="is_in"/>
    <constraint desc="" exp="" field="anglican"/>
    <constraint desc="" exp="" field="barrier"/>
    <constraint desc="" exp="" field="stars"/>
    <constraint desc="" exp="" field="source:amenity"/>
    <constraint desc="" exp="" field="source:name"/>
    <constraint desc="" exp="" field="bicycle_parking"/>
    <constraint desc="" exp="" field="covered"/>
    <constraint desc="" exp="" field="public_transport"/>
    <constraint desc="" exp="" field="toilets"/>
    <constraint desc="" exp="" field="toilets:access"/>
    <constraint desc="" exp="" field="toilets:changing_table"/>
    <constraint desc="" exp="" field="toilets:wheelchair:access"/>
    <constraint desc="" exp="" field="atm"/>
    <constraint desc="" exp="" field="atm:fee"/>
    <constraint desc="" exp="" field="ref:navads"/>
    <constraint desc="" exp="" field="owner"/>
    <constraint desc="" exp="" field="room"/>
    <constraint desc="" exp="" field="disused:shop"/>
    <constraint desc="" exp="" field="takeaway"/>
    <constraint desc="" exp="" field="name:en"/>
    <constraint desc="" exp="" field="opening_hours:url"/>
    <constraint desc="" exp="" field="games_room"/>
    <constraint desc="" exp="" field="addr:suburb"/>
    <constraint desc="" exp="" field="toilets:disposal"/>
    <constraint desc="" exp="" field="community_centre"/>
    <constraint desc="" exp="" field="craft"/>
    <constraint desc="" exp="" field="maxheight"/>
    <constraint desc="" exp="" field="place"/>
    <constraint desc="" exp="" field="old_amenity"/>
    <constraint desc="" exp="" field="old_religion"/>
    <constraint desc="" exp="" field="source_ref"/>
    <constraint desc="" exp="" field="community"/>
    <constraint desc="" exp="" field="community:gender"/>
    <constraint desc="" exp="" field="unisex"/>
    <constraint desc="" exp="" field="delivery"/>
    <constraint desc="" exp="" field="credit_cards"/>
    <constraint desc="" exp="" field="mapillary"/>
    <constraint desc="" exp="" field="drive_through"/>
    <constraint desc="" exp="" field="construction_date"/>
    <constraint desc="" exp="" field="animal_shelter"/>
    <constraint desc="" exp="" field="studio"/>
    <constraint desc="" exp="" field="golf"/>
    <constraint desc="" exp="" field="house"/>
    <constraint desc="" exp="" field="disused:name"/>
    <constraint desc="" exp="" field="nursery"/>
    <constraint desc="" exp="" field="automated"/>
    <constraint desc="" exp="" field="noname"/>
    <constraint desc="" exp="" field="self_service"/>
    <constraint desc="" exp="" field="disused:office"/>
    <constraint desc="" exp="" field="abandoned:building"/>
    <constraint desc="" exp="" field="voltage"/>
    <constraint desc="" exp="" field="naptan:AtcoCode"/>
    <constraint desc="" exp="" field="naptan:Bearing"/>
    <constraint desc="" exp="" field="naptan:BusStopType"/>
    <constraint desc="" exp="" field="naptan:CommonName"/>
    <constraint desc="" exp="" field="naptan:Indicator"/>
    <constraint desc="" exp="" field="naptan:Landmark"/>
    <constraint desc="" exp="" field="naptan:NaptanCode"/>
    <constraint desc="" exp="" field="naptan:Street"/>
    <constraint desc="" exp="" field="naptan:verified"/>
    <constraint desc="" exp="" field="historic:railway"/>
    <constraint desc="" exp="" field="addr:state"/>
    <constraint desc="" exp="" field="payment:cards"/>
    <constraint desc="" exp="" field="payment:cash"/>
    <constraint desc="" exp="" field="location"/>
    <constraint desc="" exp="" field="recycling_type"/>
    <constraint desc="" exp="" field="nohousenumber"/>
    <constraint desc="" exp="" field="historic:civilization"/>
    <constraint desc="" exp="" field="camra"/>
    <constraint desc="" exp="" field="capacity"/>
    <constraint desc="" exp="" field="dispensing"/>
    <constraint desc="" exp="" field="service:vehicle:car_repair"/>
    <constraint desc="" exp="" field="service:vehicle:new_car_sales"/>
    <constraint desc="" exp="" field="service:vehicle:used_car_sales"/>
    <constraint desc="" exp="" field="microbrewery"/>
    <constraint desc="" exp="" field="service:vehicle:body_repair"/>
    <constraint desc="" exp="" field="service:vehicle:diagnostics"/>
    <constraint desc="" exp="" field="service:vehicle:inspection"/>
    <constraint desc="" exp="" field="monitoring:weather"/>
    <constraint desc="" exp="" field="gambling"/>
    <constraint desc="" exp="" field="construction_year"/>
    <constraint desc="" exp="" field="tower:construction"/>
    <constraint desc="" exp="" field="tower:type"/>
    <constraint desc="" exp="" field="postal_code"/>
    <constraint desc="" exp="" field="addr:interpolation"/>
    <constraint desc="" exp="" field="building:colour"/>
    <constraint desc="" exp="" field="building:material"/>
    <constraint desc="" exp="" field="roof:colour"/>
    <constraint desc="" exp="" field="roof:material"/>
    <constraint desc="" exp="" field="cafe"/>
    <constraint desc="" exp="" field="content"/>
    <constraint desc="" exp="" field="nat_ref"/>
    <constraint desc="" exp="" field="nam"/>
    <constraint desc="" exp="" field="opening_hours:signed"/>
    <constraint desc="" exp="" field="bunker_type"/>
    <constraint desc="" exp="" field="military"/>
    <constraint desc="" exp="" field="roof:direction"/>
    <constraint desc="" exp="" field="government"/>
    <constraint desc="" exp="" field="addr:street_1"/>
    <constraint desc="" exp="" field="service:vehicle:MOT"/>
    <constraint desc="" exp="" field="service:vehicle:batteries"/>
    <constraint desc="" exp="" field="diaper"/>
    <constraint desc="" exp="" field="payment:american_express"/>
    <constraint desc="" exp="" field="payment:bancomat"/>
    <constraint desc="" exp="" field="payment:bankaxess"/>
    <constraint desc="" exp="" field="payment:coins"/>
    <constraint desc="" exp="" field="payment:diners_club"/>
    <constraint desc="" exp="" field="payment:discover_card"/>
    <constraint desc="" exp="" field="payment:girocard"/>
    <constraint desc="" exp="" field="payment:jcb"/>
    <constraint desc="" exp="" field="payment:laser"/>
    <constraint desc="" exp="" field="payment:maestro"/>
    <constraint desc="" exp="" field="payment:mastercard"/>
    <constraint desc="" exp="" field="payment:notes"/>
    <constraint desc="" exp="" field="payment:visa"/>
    <constraint desc="" exp="" field="payment:visa_debit"/>
    <constraint desc="" exp="" field="payment:visa_electron"/>
    <constraint desc="" exp="" field="addr:source"/>
    <constraint desc="" exp="" field="ref:pol_id"/>
    <constraint desc="" exp="" field="payment:debit_cards"/>
    <constraint desc="" exp="" field="abandoned:man_made"/>
    <constraint desc="" exp="" field="gas_insulated"/>
    <constraint desc="" exp="" field="disused:operator"/>
    <constraint desc="" exp="" field="material"/>
    <constraint desc="" exp="" field="listed"/>
    <constraint desc="" exp="" field="bench"/>
    <constraint desc="" exp="" field="shelter_type"/>
    <constraint desc="" exp="" field="source:HE_ref"/>
    <constraint desc="" exp="" field="disused:building"/>
    <constraint desc="" exp="" field="tomb"/>
    <constraint desc="" exp="" field="water_point"/>
    <constraint desc="" exp="" field="waterway"/>
    <constraint desc="" exp="" field="snooker"/>
    <constraint desc="" exp="" field="lit"/>
    <constraint desc="" exp="" field="opening_hours:kitchen"/>
    <constraint desc="" exp="" field="disused:craft"/>
    <constraint desc="" exp="" field="artist_name"/>
    <constraint desc="" exp="" field="artwork_type"/>
    <constraint desc="" exp="" field="ref:edubase"/>
    <constraint desc="" exp="" field="bin"/>
    <constraint desc="" exp="" field="indoor"/>
    <constraint desc="" exp="" field="telecom"/>
    <constraint desc="" exp="" field="water"/>
    <constraint desc="" exp="" field="generator:method"/>
    <constraint desc="" exp="" field="generator:type"/>
    <constraint desc="" exp="" field="plant:output:electricity"/>
    <constraint desc="" exp="" field="plant:source"/>
    <constraint desc="" exp="" field="reservation"/>
    <constraint desc="" exp="" field="addr:door"/>
    <constraint desc="" exp="" field="area"/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" sortExpression="" actionWidgetStyle="dropDown">
    <columns/>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable/>
  <labelOnTop/>
  <widgets/>
  <previewExpression></previewExpression>
  <mapTip></mapTip>
  <layerGeometryType>2</layerGeometryType>
</qgis>
